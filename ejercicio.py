# rut, nombre, comuna, activo/inactivo
# buscar por rut
# Comunas: Talcahuano, Concepcion, Hualpen

from os import system

system("cls")

rut = []
nombres = []
comuna = []
estado = []

def Pausa():
    print("Presione una tecla para continuar...")
    input()
    system("cls")
    
def IngresarCliente():
    rut.append(input("Ingrese el rut del cliente: "))
    nombres.append(input("Ingrese el nombre del cliente: "))
    print("Seleccione la comuna a la que pertenece el cliente")
    print("---------------------------")
    print("""1.Concepcion
2.Hualpen
3.Talcahuano""")
    print("---------------------------")
    
    op=int(input("Ingrese una opcion: "))
    
    
    if op==1:
        nomComuna="Concepcion"
    elif op==2:
        nomComuna="Hualpen"
    elif op==3:
        nomComuna="Talcahuano"
        
    comuna.append(nomComuna)
    estado.append(input("Ingrese el estado del cliente"))

def ModificarCliente():
    pass

def EliminarCliente():
    pass

def BuscarCliente():
    pass

def ContarClienteIn():
    pass

def ContarClienteCom():
    pass

def Menu():
    print("SELECCIONE UNA OPCION")
    print("""1.Ingresar cliente
2.Modificar cliente
3.Eliminar cliente
4.Buscar cliente
5.Contar clientes inactivos
6.Contar clientes por comuna
7.Salir""")
    
while True:
    Menu()
    
    op=int(input("INGRESE UNA OPCION: "))
    if op==1:
        IngresarCliente()
        
    elif op==2:
        ModificarCliente()
        
    elif op==3:
        EliminarCliente()
        
    elif op==4:
        BuscarCliente()
        
    elif op==5:
        ContarClienteIn()
        
    elif op==6:
        ContarClienteCom()
        
    elif op==7:
        print("Fin del programa")
        
    else:
        print("Opcion invalida")
        
    Pausa()
    
    
    
    